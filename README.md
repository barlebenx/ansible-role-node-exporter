# Ansible Role: node_exporter

[![pipeline status](https://gitlab.com/barlebenx/ansible-role-node-exporter/badges/master/pipeline.svg)](https://gitlab.com/barlebenx/ansible-role-node-exporter/-/commits/master)

Install node_exporter for prometheus metrics.

## Requirements

See meta for distribution requirements

## Role Variables

Check in `./defaults/main.yml`

## Dependencies

## Example Playbook

```yaml
- hosts: all
  roles:
    - node_exporter
```

## License

MIT
